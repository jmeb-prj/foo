
FROM python:3.9.1-alpine3.13 as build

ARG ANSIBLE_VERSION=2.10.0

RUN apk --update --no-cache add \
      build-base \
      libffi-dev \
      openssl-dev

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN set -e; \
    pip3 install --upgrade pip; \
    pip install --prefix="/install" ansible==${ANSIBLE_VERSION}; \
    rm -rf $(pip cache dir);

FROM python:3.9.1-alpine3.13

COPY --from=build /install /usr/local

RUN apk add --no-cache \
      bash \
      curl \
      docker-cli \
      file \
      grep \
      openssh-client \
      sed

RUN set -e; \
    pip install  \
      docker \
      jsondiff \
      requests; \
    ansible-galaxy collection install \
      community.docker \
      community.general; \
    rm -rf $(pip cache dir); \
    mkdir /docker-entrypoint.d /srv/ansible

COPY /docker-entrypoint /usr/local/bin

ENTRYPOINT [ "docker-entrypoint" ]

#ENV ANSIBLE_FORCE_COLOR=True
ENV TTY=x-term
WORKDIR /srv/ansible