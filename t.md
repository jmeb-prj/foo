# Déploiement projet Symfony [WIP]

## Principe

### Prérequis

#### Poste de déploiement

* GNU Make (disponible via le paquet `make` sur les distributions Linux)
* Docker  >= v19.03
* accès à la registry [Harbor](http://gitlab.sso.pp.minint.fr/forgepp/forgepp/blob/master/harbor.md) 
* clés SSH disponibles (cf. https://www.ssh.com/academy/ssh/keygen)

#### Serveurs cibles
* python 3
* compte utilisateur avec la clé publique associée pour connexion ssh

### Utilisation
L'exploitation du projet se fait à l'aide d'Ansible. Les playbooks suivants sont définis:

* `install_root.yml` se charge d'installer et configurer sur les serveurs d'un environement les services applicatifs (PostgreSQL, Apache et PHP, etc).
* `deploy.yml` construit une archive de l'application avec toutes les dépendances (composer, etc.) et met à jour une plateforme cible

### Arborescence

#### Organisation du projet

```shell
/
├─ source   # projet Symfony [TODO : déplacer le code actuellement à la racine]
│  ├─ config
│  ├─ src
│  ├─ public
│  └─ ...
├─ playbook
│  ├─ inventories
│  │  ├─ code_env_1      # code de l'environnement (e.g. pld)
│  │  │  ├─ group_vars
│  │  │  │  └─ all       # définition des valeurs pour l'environnement
│  │  │  │     ├─ vars.yml
│  │  │  │     └─ vault.yml
│  │  │  └─ hosts        # description des machines
│  │  ├─ code_env_2
│  │  └─ ...
│  ├─ templates
│  │  └─ .env.local.j2   # template pour le fichier d'environnement Symfony 
│  └─ vars
│     └─ main.yml  # définition des variables 
├─ Makefile        # lance les commandes de déploiement
└─ .gitlab-ci.yml  # définition des pipelines pour GitLab
```

Le fichier `playbook/vars/main.yml` contient toutes les variables nécessaires à la configuration des plateforme et de l'application (NB : si le nombre de variables est important, possibilité de séparer fonctionnelement dans plusieurs fichiers). Les variables dépendant de la plateforme font référence à une variable intermédiaire (e.g. `myvar = "{{ env_myvar }}"`)

On définit un inventaire par plateforme, on utilise le système de [surcharge par inventaire](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#organizing-host-and-group-variables) d'Ansible pour définir toutes les variables `env_*` et la [best practice suivante](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#keep-vaulted-variables-safely-visible) pour gérer les données sensibles (mots de passe, tokens, etc.).

Ces variables sont utilsées par les templates  `playbook/templates/` pour générer les différents fichiers de configuration (services de l'OS et application).

### Plateforme PLx

Le playbook d'installation configure la machine apache de la façon suivante

```
/data
├─ $utilisateur
│  ├─ param
│  │  ├─ conf        # Configuration Apache globale
│  │  ├─ conf.d      # Configuration pour les vhosts des projets
│  │  ├─ conf.modules.d  # Modules apaches spécifiques
│  │  └─ php.ini
│  ├─ current # Lien symbolique vers la release actuelle
│  └─ www     # DocumentRoot d'Apache, lien symbolique vers current/public pour Sf
└─ $utilisateur_backup
   └─ releases  # Ensemble des versions déployées
      ├─ release1
      ├─ release2
      └─ ...
```

## Configuration des scripts

Il y a 3 niveaux de configuration.

### Serveurs applicatifs

Configure les serveurs en installant les services (serveur web, php, base de données, etc.) pour un environnement. Nécessaire à l'installation des serveurs, et lors de mises à jour système (e.g. montée de version de la bdd, nouvelle extension PHP).

Se référer aux documentations des différents [rôles](http://gitlab.sso.pp.minint.fr/ansible/deploy) pour la liste des variables nécessaires.

Le rôle apache lit les fichiers dans conf.d et conf.modules.d. C'est dans ces dossiers que l'on va donc mettre les configuration Apache pour fonctionner avec PHP et définir les vhosts

### Application

Symfony utilise un système de fichier d'environnement pour la configuration. Le fichier `playbook/templates/.env.local.j2` contient un template du fichier pour l'application considérée. Ce fichier utilise des variables utilisé par Ansible pour le générer avec les valeurs propres à chaque environnement. Les variables sont chargées pour une plateforme par le mécanisme des inventaires d'Ansible. Le fichier de template + les variables permettent de générer un fichier `.env.local` qui sera déployé à la racine du projet Symfony

### Déploiement

Le fichier Makefile permet le lancement des actions de la pipeline. Il permet de simplifier et variabiliser les commandes, notamment en précisant la plateforme cible (ex. PLD, PLE) et de configurer les options pour Ansible et Docker (verbosité, montage de volume, etc.).

Ansible est exécuté via un conteneur outil (cf. [image Ansible](https://registry.pp.minint.fr/harbor/projects/49/repositories/ansible%2Fansible)) dans lequel est monté entre autres le répertoire du projet et le répertoire ~/.ssh pour permettre au conteneur de voir les playbooks

## Description de la pipeline

**Test**

* check-security
  Vérifie les packages d'un composer.lock pour remonter les éventuelles vulnérabilités. Utilise une image Docker pour [Local PHP Security Checker](https://github.com/fabpot/local-php-security-checker) contenant en cache la base de données (donc ne nécessite pas un accès Orion)
* lint
  Vérifie les règles de codage préconisées pour un projet Symfony. Utiise une image Docker avec [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)

**Build**

* composer
  A partir d'une image Docker de composer une nouvelle image temporaire est générée dans laquelle le code source du projet est ajouté et on installe les dépendances

**Deploy**

* deploy-sf
  A partir de l'image précédente on récupère le projet complet pour générer une archive à déployer sur une machine. Cette archive est consommée par un playbook Ansible qui effectue les actions suivantes :
  * préparation d'une nouvelle release (création d'un répertoire du nom de la release - dans `/data/ges-sf_backup/releases`)
  * décompression de l'archive dans le dossier de la release
  * création du fichier `.env.local` placer également dans le répertoire pour configurer les variables propres à la plateforme. Ce fichier est généré à partir du fichier `playbook/templates/.env.local.j2` dont les variables sont chargées automatiquement en fonction de la plateforme déployée (système d'inventaire et surcharge de variables d'Ansible cf. )
  * lancement des migrations Doctrine
  * basculement du lien symbolique `current` sur la nouvelle release
  * chargement des données de test en base - optionnel (déclenchement manuel)
